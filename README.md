Use `mvn` to install this package manually:

```
git clone https://gitlab.com/hometown-quotes/velocify-api
mvn install:install-file \
    -Dfile=velocify-api/velocify-api/1.0.0/velocify-api-1.0.0.jar \
    -DpomFile=velocify-api/velocify-api/1.0.0/velocify-api-1.0.0.pom
```